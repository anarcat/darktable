#ifndef RC_BUILD
  #ifdef HAVE_CONFIG_H
    #include "config.h"
  #endif
  const char darktable_package_version[] = "2.4.3";
  const char darktable_package_string[] = PACKAGE_NAME " 2.4.3";
  const char darktable_last_commit_year[] = "2018";
#else
  #define DT_MAJOR 2
  #define DT_MINOR 4
  #define DT_PATCH 3
  #define DT_N_COMMITS 0
  #define LAST_COMMIT_YEAR "2018"
#endif
